<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    @yield('head')
</head>
<body>
    <header>
        <div class="logo">
            <a href="{{ route('app.index') }}">LaraPanel</a>
        </div>
    </header>
    <main>
        @yield('content')
    </main>


    <script src="{{ asset('script.js') }}"></script>
    @yield('script')
</body>
</html>
