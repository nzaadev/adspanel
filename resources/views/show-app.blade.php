@extends('template')



@section('content')

    @php

        $keys = [];
        if ($app != null) {
            $data = json_decode($app->setting->data_setting, true);
            $keys = array_keys($data);
        }

    @endphp

    <div>


        <div class="data-app">
            <form method="POST" action="{{ route('app.update', ['app' => $app->id ]) }}">
                <div>
                    <input type="submit" value="Update App info">
                </div>
                <input type="hidden" name="type" value="app">
                @csrf

                <label for="">URL</label>
                <div style="color: green; font-weight: bold; display: flex">
                    <input id="myInput" style="width: 50%" readonly type="text" value="{{ route('app.api',  [ 'app' => $app->app_package ]) }}">
                    <a target="_blank" href="{{ route('app.api',  [ 'app' => $app->app_package ]) }}">show</a>
                    <a class="btn" onClick="copyApp()">Copy URL</a>
                </div>
                <label for="">App Name</label>
                <input type="text" name="app_name" value="{{ $app->app_name }}">

                <label for="">Package</label>
                <input type="text" name="app_package" value="{{ $app->app_package }}">
            </form>
        </div>

        <div style="margin-top: 20px; margin-bottom: 20px; background-color: gray">-</div>

        <div class="button-data">
            <button id="toggle-setting" class="btn">Show Setting</button>
            <button id="toggle-data" class="btn">Show Data</button>
            <form style="display: inline-block;" action="{{ route('app.destroy', [ 'app' => $app->id ]) }}">
                @csrf
                <input class="btn" style="background-color: #dd0000" type="submit" value="Delete App">
            </form>
        </div>




        <div class="data-setting hidden">
            <h3>Settings {{ $app->app_name }}</h3>
            <form method="POST" action="{{ route('app.update', ['app' => $app->id]) }}">
                <div>
                    <input type="submit" value="Update Settings">
                </div>
                @csrf
                <input type="hidden" name="type" value="setting">
                @forelse ($keys as $item)
                    <label for="">{{ ucfirst(str_replace('_', ' ', $item)) }}</label>
                    @if ($item == 'select_main_ads' || $item == 'select_backup_ads')
                        <select name="{{ $item }}" id="">
                            @foreach ($ads_network as $itemz)
                                <option value="{{ $itemz }}" {{ $itemz == $data[$item] ? 'selected' : '' }}>
                                    {{ $itemz }}</option>
                            @endforeach
                        </select>
                    @else
                        <input name="{{ $item }}" type="text" value="{{ $data[$item] }}">
                    @endif
                @empty
                @endforelse

            </form>
        </div>

        <div class="data-content hidden">
            <h3>Content {{ $app->app_name }}</h3>
            mode: <button class="mode" onclick="code()">code</button> <button class="mode" onclick="tree()">tree</button> <button class="mode" onclick="text()">text</button>
            <form method="POST" action="{{ route('app.update', ['app' => $app->id]) }}">
                <div style="margin-bottom: 10px">
                    <input type="submit" value="Update Data">
                </div>
                @csrf
               <div>
                    <input type="hidden" name="type" value="content">
                    <textarea style="display: none" name="data_content" id="data_content" cols="30" rows="10">{{ $app->setting->data_content }}</textarea>
                    <div style="height: 500px" id="jsoneditor"></div>
               </div>


            </form>
        </div>

    </div>


@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/jsoneditor@9.10.0/dist/jsoneditor.min.js"></script>

<script>
    // create the editor
    const textarea = document.getElementById("data_content")
    const container = document.getElementById("jsoneditor")
    const options = {
        onChange: function() {
            const updatedJson = editor.get()
            textarea.innerText = JSON.stringify(updatedJson)
        }
    }
    const editor = new JSONEditor(container, options)


    // set json
    {!! !empty($app->setting->data_content) ? "const initialJson = JSON.parse('".$app->setting->data_content."')\n  editor.set(initialJson)" : '' !!}



    function code() {
        editor.setMode('code')
    }

    function tree() {
        editor.setMode('tree')
    }

    function text() {
        alert('Hati2 mode text bisa sebabkan error')
        editor.setMode('text')
    }

    function copyApp() {

        var copyText = document.getElementById("myInput");

        // Select the text field
        copyText.select();
        copyText.setSelectionRange(0, 99999); // For mobile devices

        // Copy the text inside the text field
        navigator.clipboard.writeText(copyText.value);

        // Alert the copied text
        alert("Copied the text: " + copyText.value);

        return false;
    }

</script>
@endsection

@section('head')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jsoneditor@9.10.0/dist/jsoneditor.min.css">
@endsection
