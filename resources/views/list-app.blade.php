@extends('template')



@section('content')
@if(Session::has('message'))
<div class="alert">
    {{ Session::get('message') }}
</div>
@endif

<div>
    <a class="btn" href="{{ route('app.create')}}">Create App</a>
</div>

@forelse ($apps as $app)
    <div class="app">
        <img src="{{ empty($app->app_logo) ?  asset('android.png') :  asset('storage/'.$app->app_logo) }}" alt="">
        <div>
            <h4>{{ $app->app_name }}</h4>
            <div>{{ $app->app_package }}</div>
            <a class="show-btn" href="{{ route('app.show', ['id' => $app->id]) }}">show app</a>
        </div>
    </div>
@empty

@endforelse
@endsection
