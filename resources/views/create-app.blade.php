@extends('template')

@section('content')

    <h2>Create App</h2>
    <form method="post" class="form" enctype="multipart/form-data" action="{{ route('app.store') }}">
        @csrf
        <label for="">App Name</label>
        <input placeholder="Genzo app" type="text" name="app_name">

        <label for="">Package</label>
        <input placeholder="com.app" type="text" name="app_package">

        <label for="">Logo</label>
        <input type="file" name="app_logo" accept="image/*" id="">

        <label for="">json ads template</label>
        <select name="ads_template" id="">
            <option value="">-- select --</option>
            @foreach (array_keys($ads_template) as $item)
            <option value="{{ $item }}">{{ $item }}</option>
            @endforeach
        </select>


        <div>
            <input type="submit" value="save">
        </div>
    </form>
@endsection
