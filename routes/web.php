<?php

use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AppController::class, 'index'])->name('app.index');
Route::get('/create-app', [AppController::class, 'create'])->name('app.create');
Route::post('/store-app', [AppController::class, 'store'])->name('app.store');
Route::post('/update-app/{app}', [AppController::class, 'update'])->name('app.update');
Route::get('/show-app/{id}', [AppController::class, 'show'])->name('app.show');
Route::get('/delete-app/{app}', [AppController::class, 'destroy'])->name('app.destroy');
Route::get('/app/{app}', [AppController::class, 'api'])->name('app.api');

