<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    use HasFactory;
    protected $guarded = [];

    function setting() {
        return $this->hasOne(Setting::class, 'app_id', 'id');
    }
}
