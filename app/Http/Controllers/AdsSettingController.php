<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAdsSettingRequest;
use App\Http\Requests\UpdateAdsSettingRequest;
use App\Models\AdsSetting;

class AdsSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAdsSettingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdsSettingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AdsSetting  $adsSetting
     * @return \Illuminate\Http\Response
     */
    public function show(AdsSetting $adsSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AdsSetting  $adsSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(AdsSetting $adsSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAdsSettingRequest  $request
     * @param  \App\Models\AdsSetting  $adsSetting
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdsSettingRequest $request, AdsSetting $adsSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AdsSetting  $adsSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdsSetting $adsSetting)
    {
        //
    }
}
