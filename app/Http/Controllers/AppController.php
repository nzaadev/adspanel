<?php

namespace App\Http\Controllers;


use App\Models\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AppController extends Controller
{

    protected $ads_template = ['alien1' => '{
        "select_main_ads": "ADMOB",
        "select_backup_ads": "FACEBOOK",
        "main_ads_banner": "ca-app-pub-3883793806331369/8277309566",
        "main_ads_intertitial": "ca-app-pub-3883793806331369/1544342268",
        "main_ads_natives": " ca-app-pub-3883793806331369/9231260597",
        "main_ads_rewards": "ca-app-pub-3883793806331369/7918178928",
        "backup_ads_natives": "",
        "backup_ads_banner": "255971253570957_255971446904271",
        "backup_ads_intertitial": "255971253570957_255971466904269",
        "backup_ads_rewards": "",
        "open_ads_admob": "ca-app-pub-3883793806331369/2781402318",
        "open_ads_alien": "xxx",
        "open_ads_applovin": "xxx",
        "switch_open_ads": "1",
        "switch_banner_natives_ads": "1",
        "interval_rewards": "2",
        "interval_native": "2",
        "interval_intertitial": "1",
        "coin_video_ads": "100",
        "coin_skin": "100",
        "coin_map": "100",
        "native_size": "1",
        "app_type_reward": "REWARD",
        "initialize_sdk": "",
        "initialize_sdk_backup_ads": "",
        "high_paying_keyword_1": "INSURANCE",
        "high_paying_keyword_2": "GAS",
        "high_paying_keyword_3": "LAWYER",
        "high_paying_keyword_4": "DONATE",
        "high_paying_keyword_5": "LOANS",
        "status_app": "0",
        "link_redirect": "https://google.com",
        "protect_app": "true",
        "base_64": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsYU65okTIwxsiWrK+i1hn2LcTG7fSWOnSCxk1w2cYubrFdIQhD33g4TAcEAu4ti7ZFwgHGH41lPWxZXvGPEwGwgj3rUu/tVj4u895vqbTAknsszuxHahgGoIPKzoFp7ZpdYmq8hPDki5fqPOZGbgkwc67ZXzO3mZlcvFV5EBT3ngG8MJFLQhvKf1TfLVGcQYBo1LPv1OKf86RjynNKoMx/18cpd/OFR0rB3MwJ0dxFES9C5xvVjKur40N4hDFSBqa1u/PnBjR14MLsFFJrzNa2Ty572wlluhY4RHuiQkREX6+sRN0LyV+QSEWV5M4YwTrDsfrd4/QR6Xvf7pzqcstQIDAQAB"
    }',
    'alien2' => '{}'
    ];

    protected $ads_network = [
        "ADMOB",
        "APPLOVIN-M",
        "APPLOVIN-D",
        "STARTAPP",
        "MOPUB",
        "IRON",
        "FACEBOOK",
        "GOOGLE-ADS",
        "UNITY"
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apps = App::all();
        return view('list-app', compact('apps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-app', ['ads_template' => $this->ads_template]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAppRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        // dd($path);

        $app = new App();
        $app->app_name = $request->app_name;
        $app->app_package = $request->app_package;
        if(!empty($request->file('app_logo'))) {
            $path = $request->file('app_logo')->store('public');
            $app->app_logo = str_replace('public/', '', $path);
        }
        $app->save();
        $app->setting()->create([
            'data_setting' => $this->ads_template[$request->ads_template],
            'data_content' => '-'
        ]);



        return redirect(route('app.index'))->with('message', 'App berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\App  $App
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $app = App::find($id);

        // dd($app);

        return view('show-app', ['app' => $app, 'ads_network' => $this->ads_network]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\App  $App
     * @return \Illuminate\Http\Response
     */
    public function edit(App $App)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAppRequest  $request
     * @param  \App\Models\App  $App
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, App $app)
    {



        if($request->type == 'setting') {

            $filter = json_encode($request->except(['_token', 'type']), JSON_UNESCAPED_SLASHES);
            $app->setting()->update(['data_setting' => $filter]);
            $app->save();
            return redirect(route('app.index'))->with('message', 'App setting berhasil diupdate');

        } else if($request->type == 'app') {

            $filter = json_encode($request->except(['_token', 'type']), JSON_UNESCAPED_SLASHES);
            $app->update([
                'app_name' => $request->app_name,
                'app_package' => $request->app_package,

            ]);
            return redirect(route('app.index'))->with('message', 'App infp berhasil diupdate');

        } else {

            $app->setting()->update(['data_content' => $request->data_content]);
            $app->save();
            return redirect(route('app.index'))->with('message', 'App data berhasil diupdate');

        }



    }

    public function api($packageName)
    {
        $app = App::with('setting')->where('app_package', $packageName)->first();
        $setting = json_decode($app->setting->data_setting, true);
        $content = json_decode($app->setting->data_content, true);


        $new = array_merge(["Ads" => $setting], $content);

        return response($new);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\App  $App
     * @return \Illuminate\Http\Response
     */
    public function destroy(App $app)
    {
        $file = public_path('storage/'.$app->app_logo);
        $app->delete();
        if (File::exists($file)) {
            File::delete($file);
        }
        return redirect(route('app.index'))->with('message', 'App data berhasil didelete');
    }
}
